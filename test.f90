module tasks
  implicit none
  type:: tcb !task control block 
     procedure(my_interface),NOPASS,pointer:: f_ptr => null() !the function pointer 
  end type tcb
  interface 
     subroutine my_interface(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
          &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
          &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
       import :: tcb
       INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
       real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
       integer ,allocatable, dimension(:),intent(inout) ::p_element
       REAL*8 ,allocatable, dimension(:),intent(inout) ::u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
            &grad_t_u,grad_t_f,grad_x_f,flux,sm
       real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
       REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
       class(tcb)::self
     end subroutine my_interface
  end interface

contains


  !------------------------------------
  real(8) function w(a,b,c)
    real(8) :: a,b,c

    w = dabs(a)**c/( dabs(a)**c+dabs(b)**c+1.0d-30)
  end function w
  !------------------------------------
  real(8) function ww0(a,b,c)
    real(8) :: a,b,c

    ww0 = (b*dabs(a)**c+a*dabs(b)**c)/( dabs(a)**c+dabs(b)**c+1.0d-30)
  end function ww0
  !------------------------------------
  real(8) function w0(a,b,c,beta)
    real(8) :: a,b,beta,c

    w0 = (1.0d0-beta)*ww0(a,b,0.0d0)+beta*ww0(a,b,c)
  end function w0
  
  subroutine u_prime_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas,cpt,2
       u_prime(i) = tab0(i,2)+dt/2.0d0*grad_t_u(i)
    end do
  end subroutine u_prime_1


  subroutine u_prime_droite_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,grad_t_u,&
         &grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:) ,intent(inout):: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2
       u_prime_plus(i)= (u_prime(i+1)-tab(i,2))/(dx/2.0d0)
    enddo
  end subroutine u_prime_droite_1

  subroutine u_prime_gauche_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas+1,cpt-1,2
       u_prime_moins(i)= -(u_prime(i-1)-tab(i,2))/(dx/2.0d0)   
    enddo
  end subroutine u_prime_gauche_1

  subroutine u_grad_x_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2
       grad_x_u(i)= w0(u_prime_plus(i),u_prime_moins(i),2.0d0,1d0)!0.99d0)
    enddo
  end subroutine u_grad_x_1


  subroutine u_prime_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas,cpt,2
       u_prime(i) = tab0(i,2)+dtsur2*grad_t_u(i)
    end do
  end subroutine u_prime_2


  subroutine taux_(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas,cpt,2
       taux(i)= dabs(tab(i,3))*dtsurdx2
    enddo
  end subroutine taux_

  subroutine u_prime_gauche_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas+1,cpt-1,2
       u_prime_moins(i) = u_prime(i-1) +(1.0d0-(taux(i-1)))*dxsur4*grad_x_u(i-1)

    enddo
  end subroutine u_prime_gauche_2



  subroutine u_prime_droite_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2
       u_prime_plus(i)  = u_prime(i+1) -(1.0d0-(taux(i+1)))*dxsur4*grad_x_u(i+1)
    enddo
  end subroutine u_prime_droite_2


  subroutine ax_droite_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2
       ax_plus(i) =  (u_prime_plus(i)- tab(i,2))/(1.0d0+(taux(i)))
    enddo
  end subroutine ax_droite_1

  subroutine ax_gauche_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas+1,cpt-1,2
       ax_moins(i) = (tab(i,2)-u_prime_moins(i))/(1.0d0+(taux(i)))
    enddo
  end subroutine ax_gauche_1

  subroutine ux_droite_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas+1,cpt-1,2
       
       ux_plus(i)  = 0.5d0*(u_prime(i+1)-tab(i,2))
    enddo
  end subroutine ux_droite_1


  subroutine ux_gauche_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self 


    do i=pas+1,cpt-1,2

       ux_moins(i) = 0.5d0*(tab(i,2)-u_prime(i-1))

    enddo
  end subroutine ux_gauche_1
  
  
  subroutine u_grad_x_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self


    do i=pas+1,cpt-1,2
       grad_x_u(i)= quatresurdx* (w(ux_plus(i),ux_moins(i),2.0d0)*ax_moins(i) &
            +  w(ux_moins(i),ux_plus(i),2.0d0)*ax_plus(i))       
    enddo    
  end subroutine u_grad_x_2

  subroutine u_prime_gauche_3(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       u_prime_moins(k) = u_prime(k+pas-2) +(1.0d0-(taux(k+pas-2)))*dxsur4*grad_x_u(k+pas-2)

    enddo
  end subroutine u_prime_gauche_3

  subroutine u_prime_droite_3(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       u_prime_plus(k)  = u_prime(k+pas-1) -(1.0d0-(taux(k+pas-1)))*dxsur4*grad_x_u(k+pas-1)
    enddo

  end subroutine u_prime_droite_3


  subroutine ax_droite_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout)::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       ax_plus(k) =  (u_prime_plus(k)- tab(k,2))/(1.0d0+(taux(k)))
    enddo
  end subroutine ax_droite_2

  subroutine ax_gauche_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       ax_moins(k) = (tab(k,2)-u_prime_moins(k))/(1.0d0+(taux(k)))
    enddo
  end subroutine ax_gauche_2

  subroutine ux_droite_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
   
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       ux_plus(k)  = 0.5d0*(u_prime(k+pas-1)-tab(k,2))

    enddo
  end subroutine ux_droite_2

  subroutine ux_gauche_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       ux_moins(k) = 0.5d0*(tab(k,2)-u_prime(k+pas-2))

    enddo
    
  end subroutine ux_gauche_2

  subroutine u_grad_x_3(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do j=2,nb_element
       k = p_element(j)
       grad_x_u(k)= quatresurdx* (w(ux_plus(k),ux_moins(k),2.0d0)*ax_moins(k) &
            +  w(ux_moins(k),ux_plus(k),2.0d0)*ax_plus(k))

    enddo
  end subroutine u_grad_x_3

  subroutine flux_(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas,cpt,2
       flux(i)= tab0(i,3)*grad_x_u(i)
    end do
  end subroutine flux_


  subroutine sm_(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas,cpt,2
       sm(i)= dtsurdx*flux(i) + dt2sur4dx*grad_t_f(i) + dxsur4*grad_x_u(i)
    end do
  end subroutine sm_

  subroutine tab_(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas,cpt,2
       tab(i,2)= 0.5d0*( tab0(i+1,2)+tab0(i-1,2)+sm(i-1)-sm(i+1))    !-------Coord paires     
    end do
  end subroutine tab_


  subroutine f_grad_x_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2

       grad_x_f(i)= tab(i,3)*(tab(i+1,2)-2.0*tab(i,2)+tab(i-1,2))*invdx2!(Différence centrée deuxième ordre (dérivée seconde de la Temp)

    enddo
  end subroutine f_grad_x_1

  subroutine u_grad_t_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self
    do i=pas+1,cpt-1,2


       grad_t_u(i) = -grad_x_f(i)+tab(i,7)*grad_x_u(i)*tab(i,8)*0.5d0!(Terme source ajouté ici seulement - Légère influence sur les interfaces)


    enddo
  end subroutine u_grad_t_1

  subroutine f_grad_t_1(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout)::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do i=pas+1,cpt-1,2

       grad_t_f(i) = grad_x_f(i)*tab(i,3)*invcfldx!(Terme en dx/dt remplacé par tab(i,3)/(cfl*dx))

    enddo
  end subroutine f_grad_t_1



  subroutine f_grad_x_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do j=2,nb_element
       i = p_element(j)
       grad_x_f(i)=(grad_x_f(i+1)+grad_x_f(i-1))*0.5d0

    enddo
  end subroutine f_grad_x_2



  subroutine u_grad_t_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do j=2,nb_element
       i = p_element(j)      
       grad_t_u(i)=(grad_t_u(i+1)+grad_t_u(i-1))*0.5d0
    enddo
  end subroutine u_grad_t_2


  subroutine f_grad_t_2(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
       &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
       &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
    INTEGER ::ww,i,j,pas,cpt ,nb_element,cpt1,k
    real*8  :: dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,dtsurdx,invcfldx,invdx2
    integer ,allocatable, dimension(:),intent(inout) ::p_element
    REAL*8 ,allocatable, dimension(:),intent(inout) :: u_prime,u_prime_moins, u_prime_plus,taux,grad_x_u,&
         &grad_t_u,grad_t_f,grad_x_f,flux,sm
    real*8,allocatable,dimension(:),intent(inout) :: ax_plus,ax_moins,ux_moins,ux_plus
    REAL*8 ,allocatable, dimension(:,:),intent(inout) ::tab0,tab
    class(tcb)::self

    do j=2,nb_element
       i = p_element(j)
       grad_t_f(i)=(grad_t_f(i+1)+grad_t_f(i-1))*0.5d0
    enddo
  end subroutine f_grad_t_2

end module tasks

module procedures
contains
  SUBROUTINE DIFFUSIVE(nb_element,cpt,dx,p_element,i_element,diffusivite,rho,cp,lambda,tab)
    INTEGER  ::i,j,nb_element,cpt
    real*8 :: dx
    integer ,allocatable, dimension(:)  ::p_element,i_element
    real*8 ,allocatable, dimension(:)   ::diffusivite,rho,cp,lambda
    real*8 ,allocatable, dimension(:,:) ::tab

    do j=1,nb_element-1
       do i=p_element(j),p_element(j+1)
          tab(i,3)=diffusivite(j)
          tab(i,5)=rho(j)
          tab(i,6)=cp(j)
          tab(i,7)=-lambda(j)
       enddo
    enddo

    do i=p_element(nb_element),p_element(nb_element)+i_element(nb_element)-1
       tab(i,3)=diffusivite(nb_element)
       tab(i,5)=rho(nb_element)
       tab(i,6)=cp(nb_element)
       tab(i,7)=-lambda(nb_element)
    enddo

    !------- Vraisemblablement, pas besoin de moyenner les lambdas, Cp et compagnie aux interfaces.

    !----------Les gradients de la diffusivite


    do i=2,cpt-1
       tab(i,4) = (tab(i+1,3)-tab(i-1,3))/(2*dx) !---- On s'en sert pas
       tab(i,8) = (1/(tab(i+1,5)*tab(i+1,6))-1/(tab(i-1,5)*tab(i-1,6)))/(2*dx) !---- Nul partout sauf aux interfaces (~Environ 3pts/interfaces)
    enddo
  END SUBROUTINE DIFFUSIVE
end module procedures

PROGRAM HECESE
  use tasks
  use procedures
  IMPLICIT NONE
  !--------Parametres solveur
  REAL(8), allocatable, dimension(:,:):: tab0,tab,tabi
  REAL(8), allocatable, dimension(:)  :: flux,sm,grad_x_u,grad_t_u,grad_t_f,grad_x_f,tabsh,delt,f1,f2,sh,u,rhs,jac,fluxnr,taux
  LOGICAL					            :: convergence=.false.
  LOGICAL					            :: test,test1,fin=.false.,stock=.false.
  REAL(8)					            :: dx,x,t0,t_fonte,dt,duree,t_chauffe,cfl,duree_totale,alphamax,error
  INTEGER					            :: cpt,imax,i,j,cpt1,compteur

  !--------Parametres composition
  INTEGER					            :: nb_element,num                    ! nombre d'element, numeo de l'element d'etude
  REAL(8), allocatable, dimension(:)	:: l_element                         ! longueur du materiau
  INTEGER, Allocatable, dimension(:) 	:: i_element                         ! le nombre de maille pour le materiau
  INTEGER, Allocatable, dimension(:)  :: p_element                         ! position dans le tableau
  REAL(8), allocatable, dimension(:)	:: diffusivite,rho,cp,lambda

  !--------Parametres limiteurs                                                ! variables pour calculs des gradients
  REAL(8), allocatable, dimension(:)	:: u_prime,u_prime_moins,u_prime_plus
  REAL(8), allocatable, dimension(:)	:: ax_plus,ax_moins,ux_plus,ux_moins


  INTEGER				             	:: i_ca_el,i_el_an,pas,ww
  !--------Parametres d'adimensionnement

  REAL(8)					            :: T_ref, L_ref, Time_ref,a_ref,lambda_ref,rho_ref,cp_ref

  !--------Stockage
  INTEGER				            	::nb_probe
  INTEGER, ALLOCATABLE,DIMENSION(:)	:: probe_indice,w_output
  REAL(8), ALLOCATABLE,DIMENSION(:)	:: probe_position
  REAL(8)	             				::intervalle_stockage, pas_stockage
  CHARACTER(40),ALLOCATABLE,DIMENSION(:)  :: nomfic
  REAL(8) 			            	::nombre

  !--------Newton Raphson

  REAL(8)                             :: jacobien,eps,DeltaT
  Integer     			            :: itermax,iter
  character*100        			    :: module0

  !--------variable pour mesurer le temps et variables pour limiter les calculs déjà connus
  real*8 :: omp_start,omp_end,TIME_START,TIME_end,temps
  real*8 ::dtsurdx,dt2,invdx2,invcfldx,dt2sur4dx,dtsurdx2,dxsur4,dtsur2,quatresurdx
  INTEGER :: t1,t2,ir,compteur_tempo
  type(tcb),dimension(3)::tasklist_CESE
  type(tcb),dimension(20)::tasklist_GRAD
  type(tcb),dimension(3)::tasklist_othersgrad
  type(tcb),dimension(3)::tasklist_condandave
  integer::ff
  type(tcb)::self

 
  !------Lecture du fichier control_input.dat

  open(10,file='control_inputV2.dat',status='old')

  read(10,*)
  read(10,*)nb_element
  read(10,*)num
  read(10,*)T_fonte
  read(10,*)T_chauffe
  read(10,*)T0
  read(10,*)rho_ref
  read(10,*)cp_ref
  read(10,*)lambda_ref
  read(10,*)L_ref
  read(10,*)

  ALLOCATE(l_element(nb_element),diffusivite(nb_element),i_element(nb_element),p_element(nb_element))
  ALLOCATE(rho(nb_element),cp(nb_element),lambda(nb_element))

  DO i=1,nb_element
     read(10,*)nombre,diffusivite(i),rho(i),cp(i),lambda(i)
     ! l_element(i)= dfloat(nombre)*1.0d-6
     l_element(i)= nombre*1.0d-6
     ! write(6,*)l_element(i),diffusivite(i),rho(i),cp(i),lambda(i)
     diffusivite(i) = -diffusivite(i)
  ENDDO

  read(10,*)
  read(10,*)nombre
  dx = nombre*1.0d-6
  !dx = dfloat(nombre)*1.0d-6
  read(10,*)cfl
  read(10,*)ww
  read(10,*)duree_totale

  !--------lecture parametres de stockage

  read(10,*)
  read(10,*)nb_probe,intervalle_stockage
  allocate(probe_indice(nb_probe),probe_position(nb_probe))
  allocate(w_output(nb_probe),nomfic(nb_probe))
  do i=1,nb_probe
     read(10,*)probe_position(i)
     probe_position(i)=probe_position(i)*1.0d-3
  enddo

  close(10)


  CALL CPU_TIME(TIME_START)
  !omp_start=omp_get_wtime()
  call  system_clock(count=t1, count_rate=ir)

  !--------calcul du nombre de maille maxi

  imax=0

  do i=1,nb_element
     i_element(i) = dint(l_element(i)/dx)+1
     imax = imax+i_element(i)
  enddo

  imax = imax-nb_element+1

  !--------initialisation

  allocate(tab(imax,8),tab0(imax,8),delt(imax),f1(imax),f2(imax),u(imax),rhs(imax))
  allocate(flux(imax),tabsh(imax),sh(imax),jac(imax),fluxnr(imax))
  allocate(sm(imax),taux(imax))
  allocate(grad_x_u(imax),grad_t_u(imax),grad_t_f(imax),grad_x_f(imax))

  allocate(u_prime(imax),u_prime_moins(imax),u_prime_plus(imax))
  allocate(ax_plus(imax),ax_moins(imax),ux_plus(imax),ux_moins(imax))


  grad_x_u =0.0d0
  grad_t_u =0.0d0
  grad_t_f =0.0d0
  grad_x_f =0.0d0
  flux =0.0d0
  sm =0.0d0
  tabsh = 0.0d0


  cpt=0
  x=-dx

  p_element(1) =1
  do i=1,i_element(1)
     cpt=cpt+1
     x=x+dx
     tab(cpt,1)=x
     tab(cpt,2)=T_chauffe
  enddo

  tab(i_element(1),2)=T0

  do j=2,nb_element-1
     p_element(j) =cpt
     do i=2,i_element(j)
        cpt=cpt+1
        x=x+dx
        tab(cpt,1)=x
        tab(cpt,2)=T0
     enddo
  enddo

  tab(cpt,2)=T_chauffe
  p_element(nb_element) =cpt

  do i=2,i_element(nb_element)
     cpt=cpt+1
     x=x+dx
     tab(cpt,1)=x
     tab(cpt,2)=T_chauffe
  enddo

  tab(1,2)=T_chauffe

  tab(cpt,2)=T_chauffe

  do i=1,nb_probe
     probe_indice(i)= dint(probe_position(i)/dx)+1
     nomfic(i) = 'Probe_t_'//char(i+48)//'base.dat'
     w_output(i)=11+i
     !write(6,*)dx, probe_position(i),probe_indice(i), nomfic(i),cpt
  enddo

  !--------Adimensionnement

  a_ref = lambda_ref/(rho_ref*cp_ref)

  time_ref = L_ref**2*rho_ref*cp_ref/lambda_ref
  T_ref = T_chauffe
  T_fonte = T_fonte/T_ref

  dx = dx/l_ref
  dt = dt /time_ref
  tab(:,1) = tab(:,1)/l_ref
  tab(:,2) = tab(:,2)/T_ref

  diffusivite(:) =diffusivite(:)/a_ref
  rho(:) = rho(:)/rho_ref
  cp(:) = cp(:)/cp_ref
  lambda(:) = lambda(:)/lambda_ref
  !--------------------

  call diffusive(nb_element,cpt,dx,p_element,i_element,diffusivite,rho,cp,lambda,tab)
  
  dt =10.0
  !dx = 2.0d0*dx 
  do i=1,cpt
     dt = min(dt,(dx**2)/dabs(tab(i,3)))
  enddo
  ! dt=(dx**2)/MAXVAL(dabs(tab(:,3)))

  dt = dt*cfl


  duree =0.0d0
  dx = 2.0d0*dx   !-------------- A cause du saute-mouton. Sans ce paramètre, le calcul plante
  duree_totale = duree_totale/time_ref

  !write(6,*)l_ref,a_ref,T_ref,time_ref,dt*time_ref,dx*l_ref
  !write(6,*)l_ref,a_ref,T_ref,time_ref,dt,dx

  do i=1,nb_probe
     open(w_output(i),file=nomfic(i), status ='unknown')
  enddo

  pas_stockage =0.0d0
  intervalle_stockage =intervalle_stockage/time_ref

  tab0(:,:) =tab(:,:)

  ! on crée des variables qui contiennent des résultats pour éviter de refaire les calculs
  ! à chaque itération de la boucle temporelles et au itération "cpt" correspondant
  dtsurdx=dt/dx
  dt2=dt**2
  dt2sur4dx=(dt**2)/(4.0d0*dx)
  dtsurdx2=dt/(dx**2)
  dxsur4=0.25d0*dx
  dtsur2=dt*0.5d0
  quatresurdx=4.0d0/dx
  dtsurdx2=dt/(dx**2)
  invdx2=1.0d0/(dx**2)
  invcfldx=1.0d0/(cfl*dx)


  compteur_tempo=0

  tasklist_CESE(1)%f_ptr => flux_
  tasklist_CESE(2)%f_ptr => sm_
  tasklist_CESE(3)%f_ptr => tab_



  tasklist_GRAD(1)%f_ptr => u_prime_1 !0
  tasklist_GRAD(2)%f_ptr => u_prime_droite_1 !1 
  tasklist_GRAD(3)%f_ptr => u_prime_gauche_1 !2
  tasklist_GRAD(4)%f_ptr => u_grad_x_1 !3
  tasklist_GRAD(5)%f_ptr => u_prime_2 !4
  tasklist_GRAD(6)%f_ptr => taux_ !5
  tasklist_GRAD(7)%f_ptr => u_prime_gauche_2 !6 
  tasklist_GRAD(8)%f_ptr => u_prime_droite_2 !7 
  tasklist_GRAD(9)%f_ptr => ax_droite_1 !8
  tasklist_GRAD(10)%f_ptr => ax_gauche_1 !9 
  tasklist_GRAD(11)%f_ptr => ux_droite_1 !10 
  tasklist_GRAD(12)%f_ptr => ux_gauche_1 !11
  tasklist_GRAD(13)%f_ptr => u_grad_x_2 !12
  tasklist_GRAD(14)%f_ptr => u_prime_gauche_3 !13 
  tasklist_GRAD(15)%f_ptr => u_prime_droite_3 !14 
  tasklist_GRAD(16)%f_ptr => ax_droite_2 !15 
  tasklist_GRAD(17)%f_ptr => ax_gauche_2 !16 
  tasklist_GRAD(18)%f_ptr => ux_droite_2 !17 
  tasklist_GRAD(19)%f_ptr => ux_gauche_2 !18 
  tasklist_GRAD(20)%f_ptr => u_grad_x_3 !19
  
  timeloop:  do while (.not.fin)
     compteur_tempo=compteur_tempo+1

     duree = duree+dt

     ! on déroule la boucle pas sinon gprof donne des informations que sur 1 subroutine 

     tab0(:,2)=tab(:,2)
     !----------- CE/SE
     do ff=1,3
        call tasklist_CESE(ff)%f_ptr(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
             &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
             &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)
     end do 
     !-------------- Traitement des interfaces - continuité du flux par la conductivité thermique
     do j=2,nb_element
        i = p_element(j)
        tab(i,2)=(tab(i-1,7)*tab(i-1,2)+tab(i+1,7)*tab(i+1,2))/(tab(i-1,7)+tab(i+1,7))
     enddo

     !-------------- Conditions cyclique sur la température
     tab(1,2)= 0.5d0*( tab0(2,2)+tab0(cpt-1,2)+sm(cpt-1)-sm(2))!2*dt/dx*(flux(1))-sm(2)-sm(1)+tab(2,2)!tab(2,2)!2000/T_ref!

     tab(cpt,2)=tab(1,2)!(-2*dt/dx)*flux(cpt)+sm(cpt-1)+sm(cpt)+tabi(cpt-1,2)!2000/T_ref!

!!$     goto 113

     !---------------Resolution par Newton raphson - Actuellement inutilisé, je le laisse au cas où

     error=0.0d0
     test1=.true.
     itermax =10
     rhs(:)=tab(:,2)
     iter =0
     eps=1.0d-10
     DeltaT=1.0d-10
     sh(:)=0
     f1(:)=0
     f2(:)=0
     delt(:)=0
     
     do while((test1).and.(iter.le.itermax))
        iter = iter+1
        do ff=1,20
           call tasklist_GRAD(ff)%f_ptr(self,ww,pas,cpt ,nb_element,cpt1,dt,dx,dxsur4,dtsur2,dtsurdx2,quatresurdx,dt2sur4dx,&
                &dtsurdx,invcfldx,p_element,u_prime,u_prime_moins,u_prime_plus,taux,grad_x_u,grad_t_u,grad_x_f,grad_t_f,&
                &ax_plus,ax_moins,ux_plus,ux_moins,sm,flux,tab0,tab)

        end do
        do j=2,cpt-1,2
           sh(j)=tab(j,7)*tab(j,8)*grad_x_u(j)

           fluxnr(j) = -(tab(j,2)+(dt/2)*sh(j)-rhs(j))

           Delt(j)=fluxnr(j)

           tab(j,2) =tab(j,2)+delt(j)
        enddo
        error=0.0d0

        do j=2,cpt-1!,2!1,nb_element!
           error = max(error, abs(delt(j)))
        enddo

        ! if(iter.eq.itermax)then
        !     write(6,*)'itermax'
        ! endif

        test1=abs(error).ge.eps!.or.dabs(error).eq.0.0d0

     enddo

!!$
!!$     !---------------Resolution des gradients
!!$
!!$113  continue 
!!$
!!$     CALL GRAD(ww,1,cpt,cpt1,dx,dt,dxsur4,dtsur2,dtsurdx2,quatresurdx,nb_element,p_element,u_prime,&
!!$          &u_prime_moins, u_prime_plus,taux,grad_x_u,grad_t_u,&
!!$          &ax_plus, ax_moins,ux_moins,ux_plus,tab0,tab)
!!$
!!$     call othersgrad(1,cpt,cpt1,invdx2,invcfldx,grad_t_f,grad_x_f,grad_t_u,grad_x_u,tab)


!!$     call condandave(grad_t_f,grad_x_f,grad_t_u,cpt,nb_element,p_element)

!!$
!!$     tab0(:,2)=tab(:,2)
!!$
!!$     !----------- CE/SE
!!$     call CESE(2,cpt,cpt1,dxsur4,dtsurdx,dt2sur4dx,flux,sm,tab,tab0,grad_t_f,grad_x_u)
!!$
!!$     !-------------- Traitement des interfaces - continuité du flux par la conductivité thermique
!!$     do j=2,nb_element
!!$        i = p_element(j)
!!$        tab(i,2)=(tab(i-1,7)*tab(i-1,2)+tab(i+1,7)*tab(i+1,2))/(tab(i-1,7)+tab(i+1,7))
!!$     enddo
!!$
!!$     !-------------- Conditions cyclique sur la température
!!$     tab(1,2)= 0.5d0*( tab0(2,2)+tab0(cpt-1,2)+sm(cpt-1)-sm(2))!2*dt/dx*(flux(1))-sm(2)-sm(1)+tab(2,2)!tab(2,2)!2000/T_ref!
!!$
!!$     tab(cpt,2)=tab(1,2)!(-2*dt/dx)*flux(cpt)+sm(cpt-1)+sm(cpt)+tabi(cpt-1,2)!2000/T_ref!
!!$
!!$     goto 114
!!$
!!$     !---------------Resolution par Newton raphson - Actuellement inutilisé, je le laisse au cas où
!!$
!!$     error=0.0d0
!!$     test1=.true.
!!$     itermax =10
!!$     rhs(:)=tab(:,2)
!!$     iter =0
!!$     eps=1.0d-10
!!$     DeltaT=1.0d-10
!!$     sh(:)=0
!!$     f1(:)=0
!!$     f2(:)=0
!!$     delt(:)=0
!!$
!!$     do while((test1).and.(iter.le.itermax))
!!$        iter = iter+1
!!$
!!$        Call GRAD(ww,2,cpt,cpt1,dx,dt,dxsur4,dtsur2,dtsurdx2,quatresurdx,nb_element,p_element,u_prime,&
!!$             &u_prime_moins, u_prime_plus,taux,grad_x_u,grad_t_u,&
!!$             &ax_plus, ax_moins,ux_moins,ux_plus,tab0,tab)
!!$        do j=3,cpt-1,2
!!$           sh(j)=tab(j,7)*tab(j,8)*grad_x_u(j)
!!$
!!$           fluxnr(j) = -(tab(j,2)+(dt/2)*sh(j)-rhs(j))
!!$
!!$           Delt(j)=fluxnr(j)
!!$
!!$           tab(j,2) =tab(j,2)+delt(j)
!!$        enddo
!!$        error=0.0d0
!!$
!!$        do j=2,cpt-1!,2!1,nb_element!
!!$           error = max(error, abs(delt(j)))
!!$        enddo
!!$
!!$        ! if(iter.eq.itermax)then
!!$        !     write(6,*)'itermax'
!!$        ! endif
!!$
!!$        test1=abs(error).ge.eps!.or.dabs(error).eq.0.0d0
!!$
!!$     enddo
!!$
!!$
!!$     !---------------Resolution des gradients
!!$
!!$114  continue
!!$
!!$     CALL GRAD(ww,2,cpt,cpt1,dx,dt,dxsur4,dtsur2,dtsurdx2,quatresurdx,nb_element,p_element,u_prime,&
!!$          &u_prime_moins, u_prime_plus,taux,grad_x_u,grad_t_u,&
!!$          &ax_plus, ax_moins,ux_moins,ux_plus,tab0,tab)
!!$
!!$     call othersgrad(2,cpt,cpt1,invdx2,invcfldx,grad_t_f,grad_x_f,grad_t_u,grad_x_u,tab)
!!$
!!$
!!$     call condandave(grad_t_f,grad_x_f,grad_t_u,cpt,nb_element,p_element)
!!$
!!$
!!$
!!$     !-------------- Test de la fonte de l'electrolite
!!$     test = .false.
!!$     i_ca_el = p_element(num)
!!$     i_el_an = p_element(num+1)
!!$     i=i_ca_el
!!$     do while(((i.lt.i_el_an-1).and.(.not.test)))
!!$
!!$        test=tab(i,2).le.(T_fonte)!20000)! Choix entre température d'équilibre ou température de fonte
!!$
!!$        i = i+1
!!$
!!$     enddo
!!$
!!$     if(i.ge.i_el_an-1)convergence =.true.
!!$     pas_stockage = pas_stockage+dt
!!$
!!$     if( convergence.and.(duree.lt.duree_totale))then
!!$
!!$        open(10,file='test'//char(ww+48)//'base.dat', status='unknown')
!!$
!!$        do i=1,cpt
!!$           write(10,*)tab(i,1)*l_ref,tab(i,2)*t_ref
!!$        enddo
!!$
!!$        close(10)
!!$
!!$        !stock=.true.
!!$        ! stop               
!!$        ! write(*,*)" fin1 =  ",fin
!!$
!!$        !duree=duree+1*10E6
!!$        fin=.true.
!!$     else
!!$        fin=duree.ge.duree_totale
!!$     endif
!!$     !endif
!!$     !
!!$
!!$
!!$     if(pas_stockage.ge.intervalle_stockage)then
!!$
!!$        do i=1,nb_probe
!!$           write(w_output(i),*)duree*time_ref, tab(probe_indice(i),2)*t_ref
!!$        enddo
!!$
!!$        pas_stockage =0.0d0
!!$     endif
  enddo timeloop

  CALL CPU_TIME(TIME_END)
  call  system_clock(count=t2, count_rate=ir)
  write(*,*)" duree =  ",duree,duree*time_ref
  write(*,*)"compteur temporel ",compteur_tempo
  write(*,*)"cpt ",cpt
  write(*,*)" temps d’excecution  du  programme",TIME_END-TIME_START
  temps=real(t2 - t1 ,kind =8)/real(ir,kind =8)
  write  (*,*) "temps d’exe  du  programme:",temps
  do i=1,nb_probe
     close(w_output(i))
  enddo

  deALLOCATE(l_element,diffusivite,i_element,p_element)
  deALLOCATE(rho,cp,lambda)
  deallocate(tab,tab0,delt,f1,f2,u,rhs)
  deallocate(flux,tabsh,sh,jac,fluxnr)
  deallocate(sm,taux)
  deallocate(grad_x_u,grad_t_u,grad_t_f,grad_x_f)
  deallocate(u_prime,u_prime_moins,u_prime_plus)
  deallocate(ax_plus,ax_moins,ux_plus,ux_moins)
  deallocate(probe_indice,probe_position)
  deallocate(w_output,nomfic)
END PROGRAM HECESE



